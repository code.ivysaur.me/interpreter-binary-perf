# interpreter-binary-perf

Brief performance testing of binary/decimal conversion functions in different scripting interpreters.

## Test conditions

Performed under bit noisy conditions (web browser + media player open, etc).

Each test was run 3x, reporting the best time

All tests are single-threaded.

```
cat /proc/cpuinfo | grep bogo | head -1
bogomips        : 6618.86
```

All packages are from Debian Buster except where indicated.

- Perl v5.28.1
- Python2 v2.7.16
- Python3 v3.7.3
- PHP v7.3.14-1
- Node.js v10.20.1 *(nodesource.com distribution)* 
- Ruby v2.5.5p157

## Raw loop performance

|Language     |Time(real)|Command
|-------------|----------|--------
|Perl5        |0m0.308s  |`time perl -e 'for($i = 0; $i < 10000000; ++$i) {}'`
|Python2      |0m0.502s  |`time python2 -c 'for i in range(10000000): pass'`
|Python3      |0m0.302s  |`time python3 -c 'for i in range(10000000): pass'`
|PHP          |0m0.249s  |`time php -r 'for($i = 0; $i < 10000000; ++$i);'`
|Node.js ⭐    |0m0.075s  |`time node -e 'for(let i = 0; i < 10000000; ++i);'`
|Ruby         |0m0.409s  |`time ruby -e 'for i in 0..10000000 ; end'`

## Binary to decimal

- Use 511 as sample value to bypass 8-bit interning

|Language     |Time(real)|Command
|-------------|----------|--------
|Perl5        |0m4.745s  |`time perl -e 'for($i = 0; $i < 10000000; ++$i) { oct("0b" + "111111111") }'`
|Python2      |0m1.794s  |`time python2 -c 'for i in range(10000000): int("111111111", 2)'`
|Python3      |0m1.953s  |`time python3 -c 'for i in range(10000000): int("111111111", 2)'`
|PHP          |0m13.146s |`time php -r 'for($i = 0; $i < 10000000; ++$i) bindec("111111111");'`
|Node.js ⭐    |0m0.709s  |`time node -e 'for(let i = 0; i < 10000000; ++i) { parseInt("111111111", 2) }'`
|Ruby         |0m1.313s  |`time ruby -e 'for i in 0..10000000 ; "111111111".to_i(2) ; end'`

## Decimal to binary

- Use 257 as sample value to bypass small-integer interning

|Language     |Time(real)|Command
|-------------|----------|--------
|Perl5 ⭐      |0m0.367s  |`time perl -e 'for($i = 0; $i < 10000000; ++$i) { sprintf("%b", 257) }'`
|Python2      |0m1.519s  |`time python2 -c 'for i in range(10000000): bin(257)[2:]'`
|Python2 (alt)|0m2.697s  |`time python2 -c 'for i in range(10000000): "{:b}".format(257)'`
|Python3      |0m1.988s  |`time python3 -c 'for i in range(10000000): bin(257)[2:]'`
|Python3 (alt)|0m2.196s  |`time python3 -c 'for i in range(10000000): "{:b}".format(257)'`
|PHP          |0m14.038s |`time php -r 'for($i = 0; $i < 10000000; ++$i) decbin(257);'`
|Node.js      |0m1.594s  |`time node -e 'for(let i = 0; i < 10000000; ++i) { (257).toString(2) }'`
|Ruby         |0m1.706s  |`time ruby -e 'for i in 0..10000000 ; 257.to_s(2) ; end'`
